package String_proccessing_regular_Expressions;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Пример:
 * ##Header line
 * Simple line with em
 * Simple *line* with strong
 * Line with link [Link to google](https://www.google.com) in center
 * Line *with* many *elements* and link [Link to FB](https://www.facebook.com)
 *
 * � езультат преобразования:
 * <html>
 * <body>
 * <h2>Header line</h2>
 * <p>Simple line <em>with</em> em</p>
 * <p>Simple <strong>line</strong> with strong</p>
 * <p>Line with link <a href=“https://www.google.com“>Link to google</a> in center</p>
 * <p> Line <strong>with</strong> <em>many</em> <strong>welementsith</strong>
 * <a href=“https://www.facebook.com“>Link to FB</a></p>
 * </body>
 * </html>
 */

public class Parser {
    public static void parseFile(String fileName) throws FileNotFoundException {
        File fileIn = new File(fileName);
        File fileOut = new File(fileName+"_converted");
        exists(fileIn);
        try (BufferedReader in = new BufferedReader(new FileReader(fileIn.getAbsoluteFile()));
                BufferedWriter out = new BufferedWriter(new FileWriter(fileOut.getAbsoluteFile()))) {
            String str = null;
            int num = 1;
            out.write("<html>");
            out.newLine();
            out.write("<body>");
            out.newLine();
            while ((str = in.readLine()) != null) {
                str = parseLine(str, num++);
                out.write(str);
                out.newLine();
            }
            out.write("</body>");
            out.newLine();
            out.write("</html>");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *  * 2. � еализовать Markdown парсер. Markdown это упрощенный язык разметки который используется в Wiki.
     * A) Header - Строка, которая начинается с #, ##, ### и т.д. является заголовком. Данные строки должны
     * преобразовываться в <h1>...</h1>, <h2>...</h2>, <h3>...</h3 > и т.д.
     * B) Простая строка должна обварачиваться в <p>...</p>
     * C) Emphasi - если в строке встречается ... то внутренность должна обернуться в <em>...</em>. Если в строке
     * встречается *...* то внутренность должна обернуться в <strong>...</strong>
     * D) Link - Строка вида:
     * [I'm an inline-style link](https://www.google.com) должна преобразовыватся в <a href=“https://www.google.com“>I'm an inline-style link</a>
     *
     * Примечание: Элементов вложенных друг в друга (“начало em **теперь strong, и продолжение”) быть не может!!!
     */
    private static String parseLine (String str, int n) {
        String srtConverted = null;


        //ловим header, добавляем в обычные строки теги в начало-конец строки
        if (str.charAt(0) == '#') {
            return "<h" + n + ">" + str.replaceAll("[#]+", "") + "</h" + n + ">";
        } else {
            srtConverted = "<p>" + str + "</p>";
        }
        Matcher matcher = null;

        Pattern patternLink = Pattern.compile("\\[(.+)\\]\\((https://www.[\\w\\.]+)\\)");
        matcher = patternLink.matcher(srtConverted);
        srtConverted = matcher.replaceAll("<a href=“" + "$2" + "“>" + "$1" + "</a>");

        Pattern patternEmphasiBold = Pattern.compile("\\*\\*(\\D+)\\*\\*");
        matcher = patternEmphasiBold.matcher(srtConverted);
        srtConverted = matcher.replaceAll("<strong>" + "$1" + "</strong>");

        Pattern patternEmphasiItalic = Pattern.compile("\\*(\\S+)\\*");
        matcher = patternEmphasiItalic.matcher(srtConverted);
        srtConverted = matcher.replaceAll("<em>" + "$1" + "</em>");

        return srtConverted;
    }

    /**
     * File exists exception
     */
    private static void exists(File file) throws FileNotFoundException {
        if (!file.exists()) {
            throw new FileNotFoundException(file.getName());
        }
    }
}
