package String_proccessing_regular_Expressions;

public class StringUtils {
	
	
	//функция, которая переворачивает строчку наоборот.
    // Пример: было “Hello world!” стало “!dlrow olleH”
    public static String revert(String input) {
        StringBuilder output = new StringBuilder();
        for (int i = input.length() - 1; i >= 0; i--) {
            output.append(input.charAt(i));
        }
        return output.toString();
    }

    //функция, которая определяет является ли строчка полиндромом.
    // Пример: А роза упала на лапу Азора
    public static boolean isPalindrome(String input) {
        input = input.toLowerCase().replaceAll("\\W", "");
        for (int i = 0; i < input.length() / 2; i++) {
            if (input.charAt(i) != input.charAt(input.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }

    //функция которая проверяет длину строки, и если ее длина больше 10, то оставить в строке только первые 6 символов,
    // иначе дополнить строку символами 'o' до длины 12.
    public static String sixOrTwelve(String input) {
        if (input.length() >= 10) {
            input = input.substring(0, 6);
        } else {
            do {
                input = input.concat("o");
            } while (input.length() != 12);
        }
        return input;
    }

    //функция, которая меняет местами первое и последнее слово в строчке
    public static String swapFirstAndLastWordsInLine(String input) {
        String[] massive = input.split("\\W");
        input = StringUtils.revert(input.replaceFirst(massive[0], massive[massive.length - 1].substring(0, 1).toUpperCase() +
                massive[massive.length - 1].substring(1)));
        input = input.replaceFirst(StringUtils.revert(massive[massive.length - 1]), StringUtils.revert(massive[0].toLowerCase()));
        return StringUtils.revert(input);
    }

    //функция, которая меняет местами первое и последнее слово в каждом предложении.
    //предложения могут разделятся ТОЛЬКО знаком точки)
    public static String swapFirstAndLastWordsInEverySentence(String input) {
        StringBuilder sb = new StringBuilder();
        Scanner scan = new Scanner(input+" ").useDelimiter("\\. ");
        while(scan.hasNext()) {
            sb.append(StringUtils.swapFirstAndLastWordsInLine(scan.next()));
            sb.append(". ");
        }
        return sb.toString().trim();
    }

    //функция, которая проверяет содержит ли строка только символы 'a', 'b', 'c' или нет.
    public static boolean isContainsOnlyTheseSymbols (String inString) {
        String str = inString.toLowerCase().replaceAll("[\\sa*b*c*]+", "");
        return str.equals("")? true : false;
    }

    //функция, которая определят является ли строчка датой формата MM.DD.YYYY
    public static boolean isDateFormattedMMDDYYYY (String inString) {
        return Pattern.matches("(\\d{2}.){2}\\d{4}", inString);
    }

    //функция, которая определяет является ли строчка почтовым адресом
    public static boolean isEmailAdress (String inString) {
        return Pattern.matches("[\\d[_][A-z]]+@([a-z\\d]+[.]){1,2}[a-z]{1,3}", inString);
    }

    //функция, которая находит все телефоны в переданном тексте формата +Х(ХХХ)ХХХ-ХХ-ХХ, и возвращает их в виде массива
    public static String [] parsePhoneNumbers (String in) {
        ArrayList<String> result = new ArrayList<>();
        Pattern pattern = Pattern.compile("[+]\\d[(]\\d{3}[)]\\d{3}([-]\\d{2}){2}");
        Matcher matcher = pattern.matcher(in);
        while (matcher.find()) {
            result.add(matcher.group());
        }

        return result.toArray(new String [result.size()]);
    }

}
