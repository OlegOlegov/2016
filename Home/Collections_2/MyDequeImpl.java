package Collections_2;

public class MyDequeImpl<E> implements MyDeque<E>{
    private int size = 0;
    private Node<E> first;
    private Node<E> last;

    public MyDequeImpl() {    }

   
    public void addFirst(E e) {
        Node<E> next = this.first;
        Node<E> newNode = new Node (e, null, next);
        this.first = newNode;
        if (next == null) {
            this.last = newNode;
        } else {
            next.prev = newNode;
        }
        this.size++;
    }

    @Override
    public void addLast(E e) {
        Node<E> prev = this.last;
        Node<E> newNode = new Node (e, prev, null);
        this.last = newNode;
        if (prev == null) {
            this.first = newNode;
        } else {
            prev.next = newNode;
        }
        this.size++;
    }

    @Override
    public E removeFirst() {
        Node<E> del = first;
        if (size == 0) return null;
        if (size == 1) {
            size = 0;
            first = null;
            last = null;
        } else {
            first = del.next;
            size--;
        }
        return del.element;
    }

    @Override
    public E removeLast() {
        Node<E> del = last;
        if (size == 0) return null;
        if (size == 1) {
            size = 0;
            last = null;
            first = null;
        } else {
            last = del.prev;
            size--;
        }
        return del.element;
    }

    @Override
    public E getFirst() {
        return first.element;
    }

    @Override
    public E getLast() {
        return last.element;
    }

    @Override
    public boolean contains(Object o) {
        Node<E> itr = first;
        while (itr != null) {
            if (itr.element.equals(o)) {
                return true;
            }
            itr = itr.next;
        }
        return false;
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public Object[] toArray() {
        Object[] ob = new Object[size];
        Node<E> elem = first;
        for (int i = 0; i < size; i ++) {
            ob[i] = elem.element;
            elem = elem.next;
        }
        return ob;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean containsAll(MyDeque<? extends E> deque) {
        if (deque.size() > this.size()) {
            return false;
        }
        E []deq = (E[])deque.toArray();
        for (E e : deq) {
            if (!contains(e)) {
                return false;
            }
        }
        return true;
    }


    private static class Node<E> {
        // хранимый элемент
        E element;
        // ссылка на следующий нод списка
        Node<E> next;
        // ссылка на предыдущий нод списка
        Node<E> prev;
        Node(E element, Node<E> prev, Node<E> next) {
            this.element = element;
            this.prev = prev;
            this.next = next;
        }
    }


}
