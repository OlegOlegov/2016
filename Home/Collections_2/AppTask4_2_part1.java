package Collections_2;

public class AppTask4_2_part1 {
    public static void main(String[] args) {
        MyDeque<Number> dequeFirst = new MyDequeImpl<>();
        dequeFirst.addFirst(5);
        dequeFirst.addFirst(3);
        dequeFirst.addFirst(1);
        dequeFirst.addFirst(7);
        dequeFirst.addLast(9);
        MyDeque<Number> dequeSecond = new MyDequeImpl<>();
        dequeSecond.addFirst(1);
        dequeSecond.addFirst(2);
        dequeSecond.addLast(3);
        dequeSecond.addLast(4);
        dequeSecond.addLast(5);

        //Демонстрация методов MyDeque
        System.out.println("dequeFirst = " + Arrays.toString(dequeFirst.toArray()));
        System.out.println("dequeSecond = " + Arrays.toString(dequeSecond.toArray()));
        System.out.println("dequeFirst remove first (7) = " + dequeFirst.removeFirst());
        System.out.println("dequeFirst remove last (9) = " + dequeFirst.removeLast());
        System.out.println("dequeFirst size? (3) = " + dequeFirst.size());
        System.out.println("dequeSecond size? (5) = " + dequeSecond.size());
        System.out.println("dequeFirst contains '3'? (true) = " + dequeFirst.contains(3));
        System.out.println("dequeSecond contains 'dequeFirst'? (true) = " + dequeSecond.containsAll(dequeFirst));
    }
}
