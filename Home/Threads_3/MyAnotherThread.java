package Threads_3;

public class MyAnotherThread extends Thread {
    public void run() {
        countIt();
    }

    private void countIt () {
        int i = 0;
        while (i++<10) {
            System.out.println(Count.counter1 > Count.counter2 ? "counter1(" + Count.counter1 + ") > " + "counter2("
                    + Count.counter2 + ")" : "counter1(" + Count.counter1 + ") =< " + "counter2(" + Count.counter2 + ")");
            Count.counter1++;
            try {
                this.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Count.counter2++;
        }
    }
}
