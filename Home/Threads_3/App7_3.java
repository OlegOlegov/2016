package Threads_3;

public class App7_3 {
    public static void main(String[] args) {
        //no sync
        Thread t1 = new MyAnotherThread();
        Thread t2 = new MyAnotherThread();
        Thread t3 = new MyAnotherThread();
        t1.start();
        t2.start();
        t3.start();

        //sync
//        Thread ts1 = new MyAnotherThreadSync();
//        Thread ts2 = new MyAnotherThreadSync();
//        Thread ts3 = new MyAnotherThreadSync();
//        ts1.start();
//        ts2.start();
//        ts3.start();
    }
}
