package Collections_3;

public class MyDequeImpl<E> implements MyDeque<E> {
    private int size = 0;
    private Node<E> first;
    private Node<E> last;

    public MyDequeImpl() {
    }

    @Override
    public void addFirst(E e) {
        Node<E> next = this.first;
        Node<E> newNode = new Node(e, null, next);
        this.first = newNode;
        if (next == null) {
            this.last = newNode;
        } else {
            next.prev = newNode;
        }
        this.size++;
    }

    @Override
    public void addLast(E e) {
        Node<E> prev = this.last;
        Node<E> newNode = new Node(e, prev, null);
        this.last = newNode;
        if (prev == null) {
            this.first = newNode;
        } else {
            prev.next = newNode;
        }
        this.size++;
    }

    @Override
    public E removeFirst() {
        Node<E> del = first;
        if (size == 0) return null;
        if (size == 1) {
            size = 0;
            first = null;
            last = null;
        } else {
            first = del.next;
            size--;
        }
        return del.element;
    }

    @Override
    public E removeLast() {
        Node<E> del = last;
        if (size == 0) return null;
        if (size == 1) {
            size = 0;
            last = null;
            first = null;
        } else {
            last = del.prev;
            size--;
        }
        return del.element;
    }

    @Override
    public E getFirst() {
        return first.element;
    }

    @Override
    public E getLast() {
        return last.element;
    }

    @Override
    public boolean contains(Object o) {
        Node<E> itr = first;
        while (itr != null) {
            if (itr.element.equals(o)) {
                return true;
            }
            itr = itr.next;
        }
        return false;
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public Object[] toArray() {
        Object[] ob = new Object[size];
        Node<E> elem = first;
        for (int i = 0; i < size; i++) {
            ob[i] = elem.element;
            elem = elem.next;
        }
        return ob;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean containsAll(MyDeque<? extends E> deque) {
        if (deque.size() > this.size()) {
            return false;
        }
        E[] deq = (E[]) deque.toArray();
        for (E e : deq) {
            if (!contains(e)) {
                return false;
            }
        }
        return true;
    }


    private static class Node<E> {
        // С…СЂР°РЅРёРјС‹Р№ СЌР»РµРјРµРЅС‚
        E element;
        // СЃСЃС‹Р»РєР° РЅР° СЃР»РµРґСѓСЋС‰РёР№ РЅРѕРґ СЃРїРёСЃРєР°
        Node<E> next;
        // СЃСЃС‹Р»РєР° РЅР° РїСЂРµРґС‹РґСѓС‰РёР№ РЅРѕРґ СЃРїРёСЃРєР°
        Node<E> prev;

        Node(E element, Node<E> prev, Node<E> next) {
            this.element = element;
            this.prev = prev;
            this.next = next;
        }
    }


    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl();
    }

    //РјРµС‚РѕРґ РґРѕР»Р¶РµРЅ РІРѕР·РІСЂР°С‰Р°РµС‚ РѕР±СЉРµРєС‚ РІРЅСѓС‚СЂРµРЅРЅРµРіРѕ РєР»Р°СЃСЃР° IteratorImpl:
    private class IteratorImpl implements Iterator<E> {
        Node<E> actual = first;
        Node<E> justDeleted = null;
        int counter = 0;

        // РїСЂРѕРІРµСЂСЏРµС‚, РµСЃС‚СЊ Р»Рё СЃР»РµРґСѓСЋС‰РёР№ СЌР»РµРјРµРЅС‚ РґР»СЏ РІС‹Р±РѕСЂРєРё РјРµС‚РѕРґРѕРј next
        public boolean hasNext() {
            if (counter == size()) {
                return false;
            } else return true;
        }

        // РІРѕР·РІСЂР°С‰Р°РµС‚ СЃР»РµРґСѓСЋС‰РёР№ СЌР»РµРјРµРЅС‚
        public E next() {
            E e = actual.element;
            actual = actual.next;
            justDeleted = null;
            counter++;
            return e;
        }


        // СѓРґР°Р»СЏРµС‚ СЌР»РµРјРµРЅС‚, РєРѕС‚РѕСЂС‹Р№ Р±С‹Р» РІРѕР·РІСЂР°С‰РµРЅ СЂР°РЅРµРµ РјРµС‚РѕРґРѕРј next
        public void remove() {
            if (counter == 0 || justDeleted != null) {
                throw new IllegalStateException();
            } else if (counter == 1) {
                removeFirst();
                counter--;
                if (counter < size) justDeleted = actual.prev;
            } else if (counter == size()-1) {
                removeLast();
                counter--;
                justDeleted = actual.prev;
            } else {
                justDeleted = actual.prev;
                actual.prev = justDeleted.prev;
                actual.prev.prev.next = actual.prev.prev != null? actual : null;
                size--;
                counter--;
            }
        }
    }
        /*
        РђР›Р“РћР РРўРњ:
        Р•РЎР›Р РџР•Р Р•Р” Р’Р«Р—РћР’РћРњ remove РќР• Р‘Р«Р› Р’Р«Р—Р’РђРќ РњР•РўРћР” next
        РР›Р РџР•Р Р•Р” Р’Р«Р—РћР’РћРњ remove Р‘Р«Р› Р’Р«Р—Р’РђРќ remove (РїРѕРІС‚РѕСЂРЅС‹Р№ РІС‹Р·РѕРІ remove)
        Р’Р«Р‘Р РћРЎРРўР¬ РРЎРљР›Р®Р§Р•РќРР• (С‚Р°Рє Рё РІСЃС‚Р°РІРёС‚СЊ РІ РєРѕРґ):
        throw new IllegalStateException();
        Р’ Р”РђРќРќРћРњ РњР•РЎРўР• РћРџР Р•Р”Р•Р›РРўР¬ Р РЈР”РђР›РРўР¬ РЎРћРћРўР’Р•РўРЎРўР’РЈР®Р©РР™ Р­Р›Р•РњР•РќРў
        */

}
