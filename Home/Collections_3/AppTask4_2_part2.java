package Collections_3;


public class AppTask4_2_part2 {
   public static void main(String[] args) {
       MyDeque<Number> deque = new MyDequeImpl<>();
       deque.addFirst(5);
       deque.addFirst(3);
       deque.addFirst(1);
       deque.addFirst(7);
       deque.addLast(9);
       System.out.println(Arrays.toString(deque.toArray()));
       for (Number element : deque) {
           System.out.println(element);
       }

       Iterator<Number> it = deque.iterator();

       while (it.hasNext()) {
           System.out.println(it.next());
           it.remove();
           /*
           try {
               it.remove();
           } catch (IllegalStateException e) {
               e.printStackTrace();
               break;
           }
           */
       }
       System.out.println(Arrays.toString(deque.toArray()));
   }
}