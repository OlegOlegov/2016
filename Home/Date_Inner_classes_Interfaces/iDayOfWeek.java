package Date_Inner_classes_Interfaces;

public enum iDayOfWeek {
	 Monday(0),
	    Tuesday(1),
	    Wednesday(2),
	    Thursday(3),
	    Friday(4),
	    Saturday(5),
	    Sunday(6);
	 private int dayOfWeekNumber;
	    iDayOfWeek(int dayOfWeekNumber) {
	        this.dayOfWeekNumber = dayOfWeekNumber;
	    }

	    public int getDayOfWeekNumber () {
	        return dayOfWeekNumber;
	    }

	    public static iDayOfWeek valueOf (int index) throws WrongDayException {
	        for (iDayOfWeek day : values()) {
	            if (day.getDayOfWeekNumber() == index) {
	                return day;
	            }
	        }
	        throw new WrongDayException();
	    }

}
