package Date_Inner_classes_Interfaces;

public class Car {
	String model;

    public Car (String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

}
