package Date_Inner_classes_Interfaces;

public class Date {
	private Year year;
	private Month month;
	private Day day;

	public Date(int day, int month, int year) {
		setDay(day);
		setMonth(month);
		setYear(year);
	}

	public iDayOfWeek getDayOfWeek() throws WrongDayException, WrongMonthException {

		return iDayOfWeek.valueOf(new Date(1, 1, 2001).daysBetween(this) % 7);
	}

	public int getDayOfYear() throws WrongMonthException {
		int num = 0;
		for (int i = 1; i < month.getMonthNumber(); i++) {
			num += month.getDays(i, year.isLeapYear());
		}
		return num + day.getDayNumber();
	}

	public int daysBetween(Date date) throws WrongMonthException {
		int y1 = date.getYear().getYearNumber();
		int y2 = getYear().getYearNumber();
		int y1DayNum = date.getDayOfYear();
		int y2DayNum = getDayOfYear();
		int sum = 0;
		if (y1 == y2) {
			return Math.abs(y1DayNum - y2DayNum);
		} else if (y1 > y2) {
			for (int i = y1 - y2; i > 0;) {
				sum += new Year(y2 + --i).getNumberOfDays();
			}
			return y1DayNum - y2DayNum + sum;
		} else {
			for (int i = y2 - y1; i > 0;) {
				sum += new Year(y1 + --i).getNumberOfDays();
			}
			return y2DayNum - y1DayNum + sum;
		}
	}

	public Day getDay() {
		return day;
	}

	public Month getMonth() {
		return month;
	}

	public Year getYear() {
		return year;
	}

	private void setDay(int day) {
		this.day = new Day(day);
	}

	private void setMonth(int month) {
		this.month = new Month(month);
	}

	private void setYear(int year) {
		this.year = new Year(year);
	}

	@Override
	public String toString() {
		try {
			return "Date{" + "year=" + year + ", month=" + month + ", day=" + day + ", dayOfWeek=" + getDayOfWeek()
					+ ", dayOfYear=" + getDayOfYear() + '}';
		} catch (WrongDayException | WrongMonthException e) {
			e.printStackTrace();
		}
		return null;
	}

	class Day {
		private int dayNumber;

		Day(int dayNumber) {
			this.dayNumber = dayNumber;
		}

		public int getDayNumber() {
			return dayNumber;
		}

		@Override
		public String toString() {
			final StringBuilder sb = new StringBuilder("Day{");
			sb.append("dayNumber=").append(dayNumber);
			sb.append('}');
			return sb.toString();
		}
	}

	class Month {
		private int monthNumber;

		Month(int monthNumber) {
			this.monthNumber = monthNumber;
		}

		public int getMonthNumber() {
			return monthNumber;
		}

		public int getDays(int monthNumber, boolean leapYear) throws WrongMonthException {
			switch (monthNumber) {
			case 1:
				return 31;
			case 2:
				if (leapYear) {
					return 29;
				} else {
					return 28;
				}
			case 3:
				return 31;
			case 4:
				return 30;
			case 5:
				return 31;
			case 6:
				return 30;
			case 7:
				return 31;
			case 8:
				return 31;
			case 9:
				return 30;
			case 10:
				return 31;
			case 11:
				return 30;
			case 12:
				return 31;
			default:
				throw new WrongMonthException();
			}
		}

		@Override
		public String toString() {
			final StringBuilder sb = new StringBuilder("Month{");
			sb.append("monthNumber=").append(getMonthNumber());
			sb.append('}');
			return sb.toString();
		}
	}

	class Year {
		final public static int LEAP_YEAR_DAY_COUNT = 366;
		final public static int COMMON_YEAR_DAY_COUNT = 365;
		private int yearNumber;
		private boolean leapYear;

		Year(int yearNumber) {
			this.yearNumber = yearNumber;
			leapYear = isLeap(yearNumber);
		}

		public int getYearNumber() {
			return yearNumber;
		}

		public boolean isLeapYear() {
			return leapYear;
		}

		public int getNumberOfDays() {
			if (isLeapYear()) {
				return LEAP_YEAR_DAY_COUNT;
			} else {
				return COMMON_YEAR_DAY_COUNT;
			}
		}

		private boolean isLeap(int yearNumber) {
			if (yearNumber % 4 == 0) {
				if (yearNumber % 100 == 0) {
					if (yearNumber % 400 == 0) {
						return true;
					}
					return false;
				}
				return true;
			}
			return false;
		}

		@Override
		public String toString() {
			final StringBuilder sb = new StringBuilder("Year{");
			sb.append("yearNumber=").append(yearNumber);
			sb.append(", leapYear=").append(leapYear);
			sb.append(", numberOfDays=").append(getNumberOfDays());
			sb.append('}');
			return sb.toString();
		}
	}
}
