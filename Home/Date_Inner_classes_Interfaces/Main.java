package Date_Inner_classes_Interfaces;

public class Main {
	 public static void main(String args[]) throws WrongDayException {
	        Date date1 = new Date(31, 12, 2016);
	        Date date2 = new Date(31, 12, 2014);
	        try {
	            System.out.println("date1 = \n" + date1.toString());
	            System.out.println("date2 = \n" + date2.toString());
	            System.out.println("daysBetween date1 and date2 = " + date1.daysBetween(date2));

	            System.out.println("DayOfWeek.valueOf(3) = " + iDayOfWeek.valueOf(3));
	        } catch (WrongMonthException ex) {
	            ex.printStackTrace();
	        }
	        Car car1 = new Car ("Volvo") { @Override public String toString(){return "1. car1: " + this.getModel();}};
	        Car car2 = new Car ("Fiat") { @Override public String toString(){return "2. car2: " + this.getModel();}};
	        System.out.println(car1.toString());
	        System.out.println(car2.toString());
}
}