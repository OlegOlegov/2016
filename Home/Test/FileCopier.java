package Test;

public class FileCopier {

    public static void fillDirectory(){
        String fileName = "src\\main\\resources\\test\\1.txt";
        File copyMe = new File(fileName);
        System.out.println(copyMe.getName());
        try(BufferedReader br = new BufferedReader(new FileReader(copyMe));
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File (fileName.substring(0, fileName.length()-4)+ "_copy" + fileName.substring(fileName.length()-4, fileName.length()))))){
            int i;
            while ((i = br.read()) != -1) {
                bw.write(i);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        FileCopier.fillDirectory();
    }

