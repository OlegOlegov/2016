package Threads_2;

public class BlockIllustraror implements Runnable {

    @Override
    public void run() {
        try {
            plusNum1();
            plusNum2();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void plusNum1() throws InterruptedException {
        synchronized (Res2.class) {
            synchronized (Res1.class) {
                Res2.getNum();
                Res1.getNum();
            }
        }

    }

    private void plusNum2() throws InterruptedException {
        synchronized (Res1.class) {
            synchronized (Res2.class) {
                Res1.getNum();
                Res2.getNum();
            }
        }

    }
}
