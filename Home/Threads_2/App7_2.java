package Threads_2;

public class App7_2 {
    /**
     * 2. � азработать пример взаимной блокировки двух потоков.
     */
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new BlockIllustraror());
        Thread t2 = new Thread(new BlockIllustraror());
        t1.start();
        t2.start();
        System.out.println("start ------------");
        t1.join();
        t2.join();
        System.out.println("------------ end");

    }
}
