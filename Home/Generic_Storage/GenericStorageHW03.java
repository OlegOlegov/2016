package Generic_Storage;

public class GenericStorageHW03<T> implements Crud<T> {
    private T[] list;
    private int nextPointer = 0;

    public GenericStorageHW03(int size) throws WrongStorageSizeException {
        if (size > 0) {
            list = (T[]) new Object[size];
        } else if (size <= 0) {
            throw new WrongStorageSizeException();
        }
    }

    public GenericStorageHW03() {
        list = (T[]) new Object[10];
    }

    @Override
    public void add(T t) {
        if (nextPointer == 0) {
            list[nextPointer] = t;
            nextPointer++;
        } else if (nextPointer > 0 && nextPointer < list.length) {
            list[nextPointer] = t;
            nextPointer++;
        } else if (nextPointer == list.length) {
            this.resize();
            list[nextPointer] = t;
            nextPointer++;
        }
    }

    @Override
    public T get(int index) throws WrongIndexException {
        if (index > -1 && index < list.length) {
            return list[index];
        } else {
            throw new WrongIndexException();
        }
    }

    @Override
    public <T> T[] getAll(T[] a) {
        return (T[])Arrays.copyOf(list, list.length, a.getClass());
    }

    @Override
    public void update(int index, T t) throws WrongIndexException {
        if (index > -1 && index < list.length && index < nextPointer) {
            list[index] = t;
        } else {
            throw new WrongIndexException();
        }

    }

    @Override
    public void delete(int index) throws WrongIndexException {
        if (index > -1 && index < list.length) {
            list[index] = null;
            if (index < list.length - 1) {
                remap(index);
            } else {
                nextPointer = index;
            }
        } else {
            throw new WrongIndexException();
        }
    }

    @Override
    public void delete(T t) {
        if (t != null) {
            for (int i = 0; i < list.length; i++) {
                if (list[i] != null && list[i].equals(t)) {
                    this.delete(i);
                }
            }
        }
    }

    @Override
    public int getSize() {
        return nextPointer;
    }


    //� асширялка массива
    private void resize() {
        T[] newList = (T[]) new Object[list.length + 10];
        for (int i = 0; i < list.length; i++) {
            newList[i] = list[i];
        }
        list = newList;
    }

    //упорядочение массива
    private void remap(int index) {
        for (int i = index; i < list.length-1; i++) {
            list[i] = list[i + 1];
        }
        list[list.length - 1] = null;
    }

}
