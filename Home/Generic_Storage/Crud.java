package Generic_Storage;

public interface Crud<T> {
    void add(T t);
    T get(int index) throws WrongIndexException;
    <T> T[] getAll(T[] a);
    void update(int index, T t) throws WrongIndexException;
    void delete(int index) throws WrongIndexException;
    void delete(T t);
    int getSize();
}
