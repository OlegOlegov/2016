package Threads_1;

public class MyRunnable implements Runnable{
    public void run () {
        try (InputStreamReader isr = new InputStreamReader(System.in)) {
            SimpleDateFormat fo = new SimpleDateFormat("HH:mm:ss");
            while (true) {
                System.out.println(Thread.currentThread().getName() + ", time: " + fo.format(System.currentTimeMillis()));
                Thread.currentThread().sleep(1000);
                if (isr.ready()) break;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}