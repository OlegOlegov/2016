package Threads_1;

public class MyThread extends Thread{
    public void run () {
        try (InputStreamReader isr = new InputStreamReader(System.in)) {
            SimpleDateFormat fo = new SimpleDateFormat("HH:mm:ss");
            while (true) {
                System.out.println(this.getName() + ", time: " + fo.format(System.currentTimeMillis()));
                sleep(1000);
                if (isr.ready()) break;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}