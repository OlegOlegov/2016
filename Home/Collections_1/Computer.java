package Collections_1;

public class Computer implements Comparable<Computer> {
    private String model;
    private int price;

    public Computer(String model, int price) {
        this.setModel(model);
        this.setPrice(price);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Computer{");
        sb.append("model='").append(getModel()).append('\'');
        sb.append(", price=").append(getPrice());
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Computer computer = (Computer) o;

        if (getPrice() != computer.getPrice()) return false;
        return getModel() != null ? getModel().equals(computer.getModel()) : computer.getModel() == null;

    }

    @Override
    public int hashCode() {
        int result = getModel() != null ? getModel().hashCode() : 0;
        result = 31 * result + getPrice();
        return result;
    }

    @Override
    public int compareTo (Computer o1) {
        return this.getPrice() - o1.getPrice() == 0 ? this.getModel().compareTo(o1.getModel()) : this.getPrice() - o1.getPrice();
    }
}