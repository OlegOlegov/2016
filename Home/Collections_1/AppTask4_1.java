package Collections_1;

public class AppTask4_1 {
    public static void main(String[] args) {
        Car[] cars = {new Car("Zzz", 1000), new Car("Fff", 2000), new Car("Eee", 6000), new Car("Yyy", 2000), new Car("Ooo", 4000)};
        Computer[] comps = {new Computer("Zzz", 100), new Computer("Fff", 200), new Computer("Eee", 600), new Computer("Yyy", 200), new Computer("Ooo", 400)};
        Integer[] integer = {1, 5, 6, 4, 1, 1, 5};
        String[] string = {"f6", "z4", "z1", "a1", "a5"};

        //max(cars);  //- compile error
        System.out.println(max(comps));
        System.out.println(max(integer));
        System.out.println(max(string));
    }

    public static Comparable max(Comparable [] input) {
        Comparable output = input[0];
        for (int i = 1; i < input.length; i ++) {
            for (int j = i; j < input.length; j ++) {
                output = output.compareTo(input[j]) > 0? output : input[j];
            }
        }
        return output;
    }
}
