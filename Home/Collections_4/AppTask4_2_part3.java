package Collections_4;



public class AppTask4_2_part3 {
    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl<>();
        deque.addFirst(5);
        deque.addFirst(3);
        deque.addFirst(1);
        deque.addFirst(7);
        deque.addLast(9);
        System.out.println(Arrays.toString(deque.toArray()));

        ListIterator<Number> listIt = deque.listIterator();
        //test 1
        while (listIt.hasNext())
            System.out.println(listIt.next());
        while (listIt.hasPrevious())
            System.out.println(listIt.previous());
        System.out.println(Arrays.toString(deque.toArray()));

        //test 2
//        while (listIt.hasNext()) {
//            System.out.println(listIt.next());
//            listIt.remove();
//            System.out.println(Arrays.toString(deque.toArray()));
//        }
        //test 3
        while (listIt.hasNext()) {
            System.out.println(listIt.next());
            listIt.set(8);
            System.out.println(Arrays.toString(deque.toArray()));
        }
        while (listIt.hasPrevious()) {
            System.out.println(listIt.previous());
            listIt.set(4);
            System.out.println(Arrays.toString(deque.toArray()));
        }

        //TODO refactor, mb set-logic?
    }
}
