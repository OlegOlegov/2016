package Collections_4;

public class MyDequeImpl<E> implements MyDeque<E> {
    private int size = 0;
    private Node<E> first;
    private Node<E> last;

    public MyDequeImpl() {
    }

    @Override
    public void addFirst(E e) {
        Node<E> next = this.first;
        Node<E> newNode = new Node(e, null, next);
        this.first = newNode;
        if (next == null) {
            this.last = newNode;
        } else {
            next.prev = newNode;
        }
        this.size++;
    }

    @Override
    public void addLast(E e) {
        Node<E> prev = this.last;
        Node<E> newNode = new Node(e, prev, null);
        this.last = newNode;
        if (prev == null) {
            this.first = newNode;
        } else {
            prev.next = newNode;
        }
        this.size++;
    }

    @Override
    public E removeFirst() {
        Node<E> del = first;
        if (size == 0) return null;
        if (size == 1) {
            size = 0;
            first = null;
            last = null;
        } else {
            first = del.next;
            size--;
        }
        return del.element;
    }

    @Override
    public E removeLast() {
        Node<E> del = last;
        if (size == 0) return null;
        if (size == 1) {
            size = 0;
            last = null;
            first = null;
        } else {
            last = del.prev;
            size--;
        }
        return del.element;
    }

    @Override
    public E getFirst() {
        return first.element;
    }

    @Override
    public E getLast() {
        return last.element;
    }

    @Override
    public boolean contains(Object o) {
        Node<E> itr = first;
        while (itr != null) {
            if (itr.element.equals(o)) {
                return true;
            }
            itr = itr.next;
        }
        return false;
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public Object[] toArray() {
        Object[] ob = new Object[size];
        Node<E> elem = first;
        for (int i = 0; i < size; i++) {
            ob[i] = elem.element;
            elem = elem.next;
        }
        return ob;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean containsAll(MyDeque<? extends E> deque) {
        if (deque.size() > this.size()) {
            return false;
        }
        E[] deq = (E[]) deque.toArray();
        for (E e : deq) {
            if (!contains(e)) {
                return false;
            }
        }
        return true;
    }


    private static class Node<E> {
        // С…СЂР°РЅРёРјС‹Р№ СЌР»РµРјРµРЅС‚
        E element;
        // СЃСЃС‹Р»РєР° РЅР° СЃР»РµРґСѓСЋС‰РёР№ РЅРѕРґ СЃРїРёСЃРєР°
        Node<E> next;
        // СЃСЃС‹Р»РєР° РЅР° РїСЂРµРґС‹РґСѓС‰РёР№ РЅРѕРґ СЃРїРёСЃРєР°
        Node<E> prev;

        Node(E element, Node<E> prev, Node<E> next) {
            this.element = element;
            this.prev = prev;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node<?> node = (Node<?>) o;

            return element != null ? element.equals(node.element) : node.element == null;

        }

        @Override
        public int hashCode() {
            return element != null ? element.hashCode() : 0;
        }
    }


    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl();
    }

    //РјРµС‚РѕРґ РґРѕР»Р¶РµРЅ РІРѕР·РІСЂР°С‰Р°РµС‚ РѕР±СЉРµРєС‚ РІРЅСѓС‚СЂРµРЅРЅРµРіРѕ РєР»Р°СЃСЃР° IteratorImpl:
    private class IteratorImpl implements Iterator<E> {
        Node<E> actual = first;
        Node<E> justDeleted = null;
        int counter = 0;

        // РїСЂРѕРІРµСЂСЏРµС‚, РµСЃС‚СЊ Р»Рё СЃР»РµРґСѓСЋС‰РёР№ СЌР»РµРјРµРЅС‚ РґР»СЏ РІС‹Р±РѕСЂРєРё РјРµС‚РѕРґРѕРј next
        public boolean hasNext() {
            if (counter == size()) {
                return false;
            } else return true;
        }

        // РІРѕР·РІСЂР°С‰Р°РµС‚ СЃР»РµРґСѓСЋС‰РёР№ СЌР»РµРјРµРЅС‚
        public E next() {
            E e = actual.element;
            actual = actual.next;
            justDeleted = null;
            counter++;
            return e;
        }


        // СѓРґР°Р»СЏРµС‚ СЌР»РµРјРµРЅС‚, РєРѕС‚РѕСЂС‹Р№ Р±С‹Р» РІРѕР·РІСЂР°С‰РµРЅ СЂР°РЅРµРµ РјРµС‚РѕРґРѕРј next
        public void remove() {
            if (counter == 0 || justDeleted != null) {
                throw new IllegalStateException();
            } else if (counter == 1) {
                removeFirst();
                counter--;
                if (counter < size) justDeleted = actual.prev;
            } else if (counter == size() - 1) {
                removeLast();
                counter--;
                justDeleted = actual.prev;
            } else {
                justDeleted = actual.prev;
                actual.prev = justDeleted.prev;
                actual.prev.prev.next = actual.prev.prev != null ? actual : null;
                size--;
                counter--;
            }
            /*
            РђР›Р“РћР РРўРњ:
            Р•РЎР›Р РџР•Р Р•Р” Р’Р«Р—РћР’РћРњ remove РќР• Р‘Р«Р› Р’Р«Р—Р’РђРќ РњР•РўРћР” next
            РР›Р РџР•Р Р•Р” Р’Р«Р—РћР’РћРњ remove Р‘Р«Р› Р’Р«Р—Р’РђРќ remove (РїРѕРІС‚РѕСЂРЅС‹Р№ РІС‹Р·РѕРІ remove)
            Р’Р«Р‘Р РћРЎРРўР¬ РРЎРљР›Р®Р§Р•РќРР• (С‚Р°Рє Рё РІСЃС‚Р°РІРёС‚СЊ РІ РєРѕРґ):
            throw new IllegalStateException();
            Р’ Р”РђРќРќРћРњ РњР•РЎРўР• РћРџР Р•Р”Р•Р›РРўР¬ Р РЈР”РђР›РРўР¬ РЎРћРћРўР’Р•РўРЎРўР’РЈР®Р©РР™ Р­Р›Р•РњР•РќРў
            */
        }
    }


    //part3_todo
    public ListIterator<E> listIterator() {
        return new ListIteratorImpl();
    }

    private class ListIteratorImpl extends IteratorImpl implements ListIterator<E> {
        Node<E> nextNode = first;
        Node<E> previousNode = last;
        Node<E> lastReturned = null;
        int vector = 0;

        int counter = 0;
        // РїСЂРѕРІРµСЂСЏРµС‚, РµСЃС‚СЊ Р»Рё СЃР»РµРґСѓСЋС‰РёР№ СЌР»РµРјРµРЅС‚ РґР»СЏ РІС‹Р±РѕСЂРєРё РјРµС‚РѕРґРѕРј next

        public boolean hasNext() {
            if (counter == size()) {
                return false;
            } else return true;
        }

        @Override
        public boolean hasPrevious() {
            if (counter == 0) {
                return false;
            } else return true;
        }

        // РІРѕР·РІСЂР°С‰Р°РµС‚ СЃР»РµРґСѓСЋС‰РёР№ СЌР»РµРјРµРЅС‚
        public E next() {
            E e = nextNode.element;
            lastReturned = nextNode;
            previousNode = nextNode;
            nextNode = nextNode.next;
            vector = 1;
            counter++;
            return e;
        }

        @Override
        public E previous() {
            E e = previousNode.element;
            lastReturned = previousNode;
            nextNode = previousNode;
            previousNode = previousNode.prev;
            vector = -1;
            counter--;
            return e;
        }

        @Override
        public void set(E e) {
            if (lastReturned != null) {
                lastReturned.element = e;
            } else {
                throw new IllegalStateException();
            }
        }
        //РњРµС‚РѕРґС‹ set/remove РјРѕРіСѓС‚ Р±С‹С‚СЊ РІС‹Р·РІР°РЅС‹ С‚РѕР»СЊРєРѕ РїРѕСЃР»Рµ next/previous.
        //РџРѕРІС‚РѕСЂРЅС‹Р№ РІС‹Р·РѕРІ (РїРѕРґСЂСЏРґ) set/remove РґРѕР»Р¶РµРЅ РїСЂРёРІРѕРґРёС‚СЊ Рє РІС‹Р±СЂРѕСЃСѓ РёСЃРєР»СЋС‡РµРЅРёСЏ IllegalStateException

        //TODO
        // СѓРґР°Р»СЏРµС‚ СЌР»РµРјРµРЅС‚, РєРѕС‚РѕСЂС‹Р№ Р±С‹Р» РІРѕР·РІСЂР°С‰РµРЅ СЂР°РЅРµРµ РјРµС‚РѕРґРѕРј next
        public void remove() {
            if (lastReturned == null) {
                throw new IllegalStateException();
            } else {
                if (vector == -1) {
                    if (counter == 1) {
                        removeFirst();
                        previousNode = null;
                    } else if (counter == size() - 1) {
                        removeLast();
                        nextNode = null;
                    } else {
                        previousNode = previousNode.prev;
                        nextNode.prev = previousNode;
                        previousNode.next = nextNode;
                    }
                } else if (vector == 1) {
                    if (counter == 1) {
                        removeFirst();
                        previousNode = null;
                    } else if (counter == size() - 1) {
                        removeLast();
                        nextNode = null;
                    } else {
                        nextNode = nextNode.next;
                        nextNode.prev = previousNode;
                        previousNode.next = nextNode;
                    }
                }
                lastReturned = null;
                counter--;
            }
        }
    }
}
