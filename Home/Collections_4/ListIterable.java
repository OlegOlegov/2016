package Collections_4;

public interface ListIterable<E> {
    ListIterator<E> listIterator();
}